﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChimeLong.Linq;
using We7.Extension.DLL.UserExtensionModel;
using ChimeLong.DLL;


namespace ChimeLong.Extension.DLL
{
    public class UserSinaWeiboService
    {
        chimelongDataContext cdc = ConnHelper.chimelong_ecard();
        /// <summary>
        /// 检测新浪用户是否已经入我UserInfo库
        /// </summary>
        /// <param name="id">SinaUser_Id</param>
        /// <returns></returns>
        public bool CheckSinaUserLoginIsExist(string sina_id)
        {
            bool result = cdc.E_Card2012_UserInfo.Where(usina => usina.SinaId == sina_id).Any();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sina_id"></param>
        /// <returns></returns>
        public WeiboUserModel GetUserModel(string sina_id)
        {
            WeiboUserModel data = new WeiboUserModel();
            var dt = cdc.E_Card2012_UserInfo.Single(ui => ui.SinaId == sina_id);
            data.id = dt.Id;
            data.name = dt.NikeName;
            data.faceimage = cdc.E_Card2012_UserFace.Single(euf => euf.UserId == dt.Id).HeadUrl;
            return data;
        }
    }
}
