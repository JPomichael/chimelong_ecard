﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace We7.Extension.DLL.UserExtensionModel
{
    public class WeiboUserModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string faceimage { get; set; }
    }
}
