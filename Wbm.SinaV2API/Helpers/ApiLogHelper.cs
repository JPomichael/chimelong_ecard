﻿/*
 This file was create by Xusion at 2011.10.27
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.IO;
using Wbm.SinaV2API.SinaServices;

namespace Wbm.SinaV2API.Helpers
{
    /// <summary>
    /// 日志对象
    /// </summary>
    public static class ApiLogHelper
    {
        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="ex">异常对象</param>
        public static void Append(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            sb.AppendLine(ex.ToString());
            sb.AppendLine(new string('-', 10));

            Save(sb.ToString());
        }

        /// <summary>
        /// 保存日志
        /// </summary>
        /// <param name="strText">日志内容</param>
        private static void Save(string strText)
        {
            string _SinaAPILogs = ""; //HttpContext.Current.Server.MapPath(SinaConfig.ApiLogPath).TrimEnd('/');

            if (System.Configuration.ConfigurationManager.AppSettings["_SinaAPILogs"] != null)
            {
                _SinaAPILogs = System.Configuration.ConfigurationManager.AppSettings["_SinaAPILogs"];
            }

            string logName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".log";
            string logPath = _SinaAPILogs + "/" + logName;
            StreamWriter sw;
            try
            {
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(_SinaAPILogs))) { Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(_SinaAPILogs)); }

                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(logPath)))
                {
                    sw = File.AppendText(System.Web.HttpContext.Current.Server.MapPath(logPath));
                }
                else
                {
                    var SavePath = System.Web.HttpContext.Current.Server.MapPath(_SinaAPILogs);
                    sw = new StreamWriter(SavePath + logName, false, System.Text.Encoding.GetEncoding("utf-8"));
                }
                sw.WriteLine(strText);
                sw.Close();
                sw.Dispose();
            }
            catch { }
        }

    }
}
