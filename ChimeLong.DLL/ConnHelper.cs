﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using ChimeLong.Linq;

namespace ChimeLong.DLL
{
    public static class ConnHelper
    {
        /// <summary>
        /// 获取数据操作接口（linq）
        /// </summary>
        /// <returns></returns>
        public static chimelongDataContext chimelong_ecard()
        {
            string chimelongConnectionString = ConfigurationManager.ConnectionStrings["chimelongConnectionString"].ToString();
            return new chimelongDataContext(chimelongConnectionString);
        }
        /// <summary>
        /// 获取数据操作接口（linq）
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <returns></returns>
        public static chimelongDataContext chimelong_ecard(string connectionString)
        {
            return new chimelongDataContext(connectionString);
        }
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns></returns>
        public static SqlConnection Conn()
        {
            string chimelong_ConnectionString = ConfigurationManager.ConnectionStrings["chimelongConnectionString"].ToString();
            return new SqlConnection(chimelong_ConnectionString);
        }
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <returns></returns>
        public static SqlConnection Conn(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
    }
}
