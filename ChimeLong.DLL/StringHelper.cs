﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;

namespace ChimeLong.DLL
{
    public static class StringHelper
    {
        /// <summary>
        /// 清除HTML标签的多余
        /// </summary>
        /// <param name="el">标签名</param>
        /// <param name="str">源字符串</param>
        ///  <param name="pat">匹配标签名</param>
        /// <param name="str">替换</param>
        /// <returns></returns>
        public static string repElement(string el, string str)
        {
            string pat = @"<" + el + "[^>]+>";
            string rep = "<" + el + ">";
            str = Regex.Replace(str.ToString(), pat, rep);
            return str;
        }

        #region 返回32位的MD5字符串
        /// <summary>
        /// 返回32位的MD5字符串
        /// </summary>
        /// <param name="md5String"></param>
        /// <returns></returns>
        public static string Tomd5(string md5String)
        {
            string strmd5;
            byte[] result = Encoding.Default.GetBytes(md5String);    //输入密码文本
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(result);
            strmd5 = BitConverter.ToString(output).Replace("-", "");  //输出加密文本
            return strmd5;
        }
        #endregion


        //public static string ContentGetThumbImg(string Content)
        //{
        //    string OriginalSrc = "";
        //    string reg = "<img[ \f\n\r\t\v]*[^>]*/?>";
        //    //  string reg = "<\"div\" class=\"newshow_pic\"" + "><\"img\" src=" + Img + " " + " style=\"Width:592px ;height:372\" > </\"div\"> ";
        //    Regex regex = new Regex(reg, RegexOptions.IgnoreCase);
        //    MatchCollection array = regex.Matches(Content.ToLower());
        //    for (int i = 0; i < array.Count; i++)
        //    {
        //        string img = array[i].Value.Replace("px", "");
        //        if (img.IndexOf("http://") >= 0 || img.IndexOf("https://") >= 0 || img.IndexOf("ftp:") >= 0 || img.IndexOf("file:") >= 0 || img.IndexOf("width=") == -1 || img.IndexOf("height=") == -1)
        //        {
        //            continue;
        //        }
        //        /*
        //         * 获取图片的宽度
        //         */
        //        regex = new Regex("width=\"[0-9]*(px)?\"", RegexOptions.IgnoreCase);
        //        int a = regex.Match(img).ToString().IndexOf('"') + 1;
        //        int b = regex.Match(img).ToString().LastIndexOf('"');
        //        int width = Convert.ToInt32(regex.Match(img).ToString().Substring(a, b - a));
        //        /*
        //        * 获取图片的高度
        //        */
        //        regex = new Regex("height=\"[0-9]*(px)?\"", RegexOptions.IgnoreCase);
        //        a = regex.Match(img).ToString().IndexOf('"') + 1;
        //        b = regex.Match(img).ToString().LastIndexOf('"');
        //        int height = Convert.ToInt32(regex.Match(img).ToString().Substring(a, b - a));
        //        /*
        //        * 获取图片的Url
        //        */
        //        regex = new Regex("src=\"[^ \f\n\r\t\v][^\"]*\"", RegexOptions.IgnoreCase);
        //        a = regex.Match(img).ToString().IndexOf('"') + 1;
        //        b = regex.Match(img).ToString().LastIndexOf('"');
        //        string src = regex.Match(img).ToString().Substring(a, b - a);

        //        OriginalSrc = src;


        //    }
        //    return OriginalSrc;
        //}

        public static string repElement(string el, string str, string Img)
        {
            string div = "div";
            string pat = @"<" + el + "[^>]+>";

            string reg = "<img[ \f\n\r\t\v]*[^>]*/?>";
            Regex regex = new Regex(reg, RegexOptions.IgnoreCase);
            MatchCollection array = regex.Matches(Img.ToLower());
            for (int i = 0; i < array.Count; i++)
            {
                string img = array[i].Value.Replace("px", "");
                if (img.IndexOf("http://") >= 0 || img.IndexOf("https://") >= 0 || img.IndexOf("ftp:") >= 0 || img.IndexOf("file:") >= 0 || img.IndexOf("width=") == -1 || img.IndexOf("height=") == -1)
                {
                    continue;
                }

                regex = new Regex("width=\"[0-9]*(px)?\"", RegexOptions.IgnoreCase);
                int a = regex.Match(img).ToString().IndexOf('"') + 1;
                int b = regex.Match(img).ToString().LastIndexOf('"');
                int width = Convert.ToInt32(regex.Match(img).ToString().Substring(a, b - a));
                a = regex.Match(img).ToString().IndexOf('"') + 1;
                b = regex.Match(img).ToString().LastIndexOf('"');
                width = width = Convert.ToInt32(regex.Match(img).ToString().Substring(a, b - a));
                /*
                * 获取图片的高度
                */
                regex = new Regex("height=\"[0-9]*(px)?\"", RegexOptions.IgnoreCase);
                a = regex.Match(img).ToString().IndexOf('"') + 1;
                b = regex.Match(img).ToString().LastIndexOf('"');
                int height = Convert.ToInt32(regex.Match(img).ToString().Substring(a, b - a));
                a = regex.Match(img).ToString().IndexOf('"') + 1;
                b = regex.Match(img).ToString().LastIndexOf('"');
                height = Convert.ToInt32(regex.Match(img).ToString().Substring(a, b - a));

                /*
                * 获取图片的Url
                */
                regex = new Regex("src=\"[^ \f\n\r\t\v][^\"]*\"", RegexOptions.IgnoreCase);
                a = regex.Match(img).ToString().IndexOf('"') + 1;
                b = regex.Match(img).ToString().LastIndexOf('"');
                string src = regex.Match(img).ToString().Substring(a, b - a);

                /*
                * 获取图片的 IMG
                */
                regex = new Regex(reg, RegexOptions.IgnoreCase);
                string oldimg = regex.Match(img).ToString();
                int c = regex.Match(img).ToString().IndexOf('<') + 1;
                int d = regex.Match(img).ToString().LastIndexOf('>');
                string old = regex.Match(img).ToString().Substring(c, d - c);
                string rep = "<" + div + " class=\"newshow_pic\"" + "><div class=\"new_piccon\"><" + el + " src=\"" + src + "\" Width=\"592px\" height=\"372px\" > </div></" + div + "> ";
                Img = Img.Replace(oldimg, rep);
            }



            //string rep = "<" + div + " class=\"newshow_pic\"" + "><" + el + " src=" + Img + " " + " style=\"Width:592px ;height:372\" > </" + div + "> ";
            //str = Regex.Replace(str.ToString(), pat, rep);
            //return str;
            return Img;
        }

        /// <summary>
        /// 把读取的文件中的所有的html标记去掉，把&nbsp;替换成空格
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string ParseHtml(string html)
        {
            html = HttpUtility.HtmlDecode(html);
            string[] el = new string[] { "p", "span", "strong", "table", "div", "tr", "td", "a", "b" };
            foreach (string s in el)
            {
                html = repElement(s, html);
            }
            //替换脚本
            html = Regex.Replace(html, @"<script(.*)</script>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML
            Regex reg = new Regex("<[^>]*>");
            html = reg.Replace(html, "");

            html = Regex.Replace(html, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"-->", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<!--.*", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"&nbsp;", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, "  ", "", RegexOptions.IgnoreCase);
            html = html.Replace("<", "");
            html = html.Replace(">", "");
            html = html.Replace("\r\n", "");
            return html;
        }
        /// <summary>
        /// 获取web.config文件中<appSetting>配置节的值
        /// </summary>
        /// <param name="Name">key</param>
        /// <returns>value</returns>
        public static string appSettings(string key)
        {
            if (key != "")
            {
                if (ConfigurationManager.AppSettings[key] != null)
                {
                    return ConfigurationManager.AppSettings[key].ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
        public static string FromatString(string html)
        {
            html = Regex.Replace(html, @"</div>", "</p>", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<div", "<p", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"</span>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<span\s*[^>]*>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"</pre>", "</p>", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<pre\s*[^>]*>", "<p>", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"</h2>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<h2>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"</h4>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<h4>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, "class=[\"][a-zA-Z0-9_]+[^\"][\"]", "", RegexOptions.IgnoreCase);
            //html = Regex.Replace(html, "style=\\s*[^\"]*", "", RegexOptions.IgnoreCase);
            //html = Regex.Replace(html, "style=\\s*[^\"]*(text-align: center;)\\s*[^\"]*", "style=\"text-align: center;\"", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"</font>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<font\s*[^>]*>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<p>&nbsp;</p>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<p><b><o:p></o:p></b></p>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"p><b>&nbsp;</b></p>", "", RegexOptions.IgnoreCase);
            //html = Regex.Replace(html, @"line-height: 150%", "line-height: 250%", RegexOptions.IgnoreCase);
            //html = Regex.Replace(html, @"line-height:150%", "line-height:250%", RegexOptions.IgnoreCase);
            return html;
        }

        /// <summary>
        /// 获取ip
        /// </summary>
        /// <returns></returns>
        public static string getRealIp()
        {
            string ip = string.Empty;
            if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_REAL_IP"].ToString();
                }
                if (!string.IsNullOrEmpty(ip))
                {
                    ip = ip.Split(',').Length > 0 ? ip.Split(',')[0] : ip;
                }
            }
            else
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            return ip;
        }
    }
}
