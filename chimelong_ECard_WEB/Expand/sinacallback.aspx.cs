﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChimeLong.Linq;
using Wbm.SinaV2API;
using ChimeLong.Extension.DLL;
using ChimeLong.DLL;


namespace chimelong_ECard_WEB.Expand
{
    public partial class sinacallback : System.Web.UI.Page
    {
        chimelongDataContext cdc = ConnHelper.chimelong_ecard();
        protected void Page_Load(object sender, EventArgs e)
        {
            var res = "";
            try
            {
                oAuthSina oauth = SinaBase.oAuth();
                oauth.GetAccessToken(); //获取认证信息
                SinaBase.UpdateCache(oauth.AccessToken);//缓存认证信息
                var dto = Wbm.SinaV2API.SinaControllers.UserController.GetUser();  //获取用户信息
                if (dto != null)
                {
                    try
                    {
                        //此 User 已经入库
                        var userSina = new UserSinaWeiboService();
                        if (userSina.CheckSinaUserLoginIsExist(Convert.ToString(dto.id)))
                        {
                            //存在
                            var sin = userSina.GetUserModel(dto.id.ToString());
                            Session.Add("username", sin.name);
                            Session.Add("userid", sin.id);
                            Session.Add("userfaceimg", dto.profile_image_url);
                        }
                        //add a user
                        else
                        {
                            //先加入user
                            E_Card2012_UserInfo user = new E_Card2012_UserInfo();
                            user.SinaId = dto.id.ToString();
                            user.AccessToken = oauth.AccessToken;
                            user.NikeName = dto.name;
                            user.Sex = dto.gender.ToLower() == "m" ? "男" : "女";  //不要出来 n 啊亲
                            user.Email = "";
                            user.AddTime = System.DateTime.Now;
                            user.Address = dto.location;
                            user.IpAddress = StringHelper.getRealIp();
                            user.State = true;
                            cdc.E_Card2012_UserInfo.InsertOnSubmit(user);
                            cdc.SubmitChanges();
                            //insert faceImg 
                            E_Card2012_UserFace euf = new E_Card2012_UserFace();
                            euf.UserId = user.Id;
                            euf.HeadUrl = dto.profile_image_url;
                            euf.SortNum = 1;
                            euf.IpAddress = StringHelper.getRealIp();
                            euf.State = true;
                            cdc.E_Card2012_UserFace.InsertOnSubmit(euf);
                            cdc.SubmitChanges();
                            Session.Add("username", dto.screen_name);
                            Session.Add("userid", user.Id);  //user 我库里的
                            Session.Add("userfaceimg", dto.profile_image_url);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("插入数据失败！详细原因：" + ex.ToString());
                    }
                }
                else
                {
                    res = "获取新浪数据失败了！╮(╯_╰)╭";
                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            if (string.IsNullOrEmpty(res))
            {
                Response.Redirect("~/index.shtml");   //当返回这个页面的时候 表示OK
            }
            else
            {
                Response.Write(res);
                Response.End();
                Response.Close();
            }
        }

    }
}