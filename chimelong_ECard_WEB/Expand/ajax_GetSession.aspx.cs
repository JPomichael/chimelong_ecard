﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using We7.Extension.DLL;
using ChimeLong.DLL;
using Newtonsoft.Json;
using We7.Extension.DLL.UserExtensionModel;
using ChimeLong.Linq;
using Wbm.SinaV2API;
using System.Text.RegularExpressions;

namespace chimelong_ECard_WEB.Expand
{
    /// <summary>
    /// 页面获取Session
    /// </summary>
    public partial class ajax_GetSession : System.Web.UI.Page
    {
        chimelongDataContext cdc = ConnHelper.chimelong_ecard();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Form["Type"] != null)
                {
                    if (Request.Form["Type"].ToString() == "GetSession")
                    {
                        Response.Write(WeiboUser_data());
                        Response.End();
                    }
                    else if (Request.Form["Type"].ToString() == "GetFriend")
                    {
                        Response.Write(WeiboUser_friend());
                        Response.End();
                    }
                    else if (Request.Form["Type"].ToString() == "SelFriend")
                    {
                        string key = Request.Form["Key"].ToString();
                        Response.Write(WeiboUser_Selfriend(key));
                        Response.End();
                    }
                    else if (Request.Form["Type"].ToString() == "SendWeibo")
                    {
                        string id = "";
                        if (Request.Form["ID"].ToString() != null)
                        {
                            id = Request.Form["ID"].ToString();
                            Response.Write(Weibo_Send(id));
                            Response.End();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 发布带图微博
        /// </summary>
        /// <param name="idone"></param>
        /// <param name="idtwo"></param>
        /// <param name="idtree"></param>
        /// <returns></returns>
        protected string Weibo_Send(string id)
        {
            var _IsOk = false;
            var _IsWeiboOk = false;
            var _ErrorMessage = "";
            var filePath = Server.MapPath("~/images/sinaweibo.jpg"); //图片地址
            string username = "";
            var weibo_username = "";
            string url = "活动地址: http://www.chimelong.com/activity/2012dzhk/index.shtml";
            var data = new
            {
                IsWeiboOk = _IsWeiboOk,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            try
            {
                if (Session["username"] != null)
                    weibo_username = Session["username"].ToString();
                else
                    weibo_username = "";
                if (weibo_username == "")
                {
                    _IsWeiboOk = false;
                    _IsOk = true;
                    _ErrorMessage = "未登录，不能发布微博！";
                }
                else
                {
                    var endindex = id.LastIndexOf(",");
                    id = id.Substring(0, endindex);
                    string[] sArray = Regex.Split(id, ",", RegexOptions.IgnoreCase);
                    foreach (string i in sArray)
                    {
                        var name = Wbm.SinaV2API.SinaControllers.UserController.GetUser(Convert.ToInt64(i));
                        username += "@" + name.name + " ";
                    }
                    var strSatus = Request.Form["Text"].ToString();
                    var isok = Wbm.SinaV2API.SinaControllers.StatusController.Update("To: " + username + strSatus + url, filePath);  //发送微博
                    if (isok)
                    {
                        _IsWeiboOk = true;
                        _IsOk = true;
                    }
                    else
                    {
                        _IsWeiboOk = false;
                        _IsOk = false;
                        _ErrorMessage = "微博发布失败！请再次尝试";
                    }
                }
            }
            catch (Exception ex)
            {
                _IsOk = false;
                _IsWeiboOk = false;
                _ErrorMessage = "微博发布失败！";
            }
            data = new
            {
                IsWeiboOk = _IsWeiboOk,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            return JavaScriptConvert.SerializeObject(data);
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        protected string WeiboUser_data()
        {
            WeiboUserModel weibo_user = new WeiboUserModel();
            var _IsOk = false;
            var _ErrorMessage = "";
            var data = new
            {
                Sei = weibo_user,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            try
            {
                // Sina
                if (Session["username"] != null)
                {
                    var name = Session["username"].ToString();
                    weibo_user.name = name.Length > 6 ? name.Substring(0, 6) + "..." : name;
                }
                else
                {
                    weibo_user.name = "";
                }
                if (Session["userid"] != null)
                {
                    weibo_user.id = Convert.ToInt32(Session["userid"]);
                }
                else
                {
                    weibo_user.id = 0;
                }
                if (Session["userfaceimg"] != null)
                {
                    weibo_user.faceimage = Session["userfaceimg"].ToString();
                }
                else { weibo_user.faceimage = ""; }
                if (weibo_user.name == "" || weibo_user.faceimage == "" || weibo_user.id == 0)
                {
                    _IsOk = true;
                    _ErrorMessage = "你还未登陆";
                }
                else
                {
                    _IsOk = true;
                }
                data = new
                {
                    Sei = weibo_user,
                    IsOk = _IsOk,
                    ErrorMessage = _ErrorMessage
                };
                // }
            }
            catch (Exception ex)
            {
                _IsOk = false;
                _ErrorMessage = "获取登录信息失败！";
                data = new
                {
                    Sei = weibo_user,
                    IsOk = _IsOk,
                    ErrorMessage = _ErrorMessage
                };
            }
            return JavaScriptConvert.SerializeObject(data);
        }

        /// <summary>
        /// 获取关注好友列表
        /// </summary>
        /// <returns></returns>
        protected string WeiboUser_friend()
        {
            int user_id = 0;
            var _IsOk = false;
            var _ErrorMessage = "";
            int _next_cursor = 0;
            List<FriendList> lfl = new List<FriendList>();
            var data = new
            {
                friList = lfl,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            user_id = Convert.ToInt32(Session["userid"]);    //? user_id : Convert.ToInt32(Session["userid"]);
            if (user_id != 0)
            {
                string sina_id = cdc.E_Card2012_UserInfo.Single(ui => ui.Id == user_id).SinaId;
                var dto = Wbm.SinaV2API.SinaControllers.UserController.GetFriendList(Convert.ToInt64(sina_id));  //获取关注人列表
                _next_cursor = dto.next_cursor;
                if (dto.users != null)
                {
                    int page = (dto.total_number / 50) + ((dto.total_number % 50) > 0 ? 1 : 0); //  dto.total_number / 50; 
                    for (int i = 0; i <= page; i++)
                    {
                        var dt = Wbm.SinaV2API.SinaControllers.UserController.GetFriendList(Convert.ToInt64(sina_id), _next_cursor);
                        _next_cursor = dt.next_cursor;
                        if (dt.users.Count() == 0)
                        {
                        }
                        else
                        {
                            foreach (var item in dt.users)
                            {
                                FriendList fl = new FriendList();
                                fl.id = item.id.ToString();
                                fl.name = item.name.Length > 7 ? item.name.Substring(0, 7) + "..." : item.name;
                                fl.image_url = item.profile_image_url;
                                lfl.Add(fl);
                            }
                        }
                    }
                    _IsOk = true;
                }
                else
                {
                    _IsOk = false;
                    _ErrorMessage = "亲~你凹凸曼了 新注册的微博吧！";
                }
            }
            else
            {
                _IsOk = false;
                _ErrorMessage = "你没有登陆！请登录获得更多的体验哦！";
            }
            data = new
            {
                friList = lfl,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            return JavaScriptConvert.SerializeObject(data);
        }

        /// <summary>
        /// 根据关键字查找关注好友
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected string WeiboUser_Selfriend(string key)
        {
            var _IsOk = false;
            var _ErrorMessage = "";
            var weibo_username = "";
            int next_cursor = 0;
            List<FriendList> lfl = new List<FriendList>();
            var data = new
            {
                friList = lfl,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            if (Session["username"] != null)
                weibo_username = Session["username"].ToString();
            else
                weibo_username = "";
            if (weibo_username != "")
            {
                string sina_id = cdc.E_Card2012_UserInfo.Single(ui => ui.NikeName == weibo_username).SinaId;
                var dto = Wbm.SinaV2API.SinaControllers.UserController.GetFriendList(Convert.ToInt64(sina_id));  //获取关注人列表
                next_cursor = dto.next_cursor;
                if (dto != null)
                {
                    int page = (dto.total_number / 50) + ((dto.total_number % 50) > 0 ? 1 : 0); //  dto.total_number / 50; 
                    for (int i = 1; i <= page; i++)
                    {
                        var dt = Wbm.SinaV2API.SinaControllers.UserController.GetFriendList(Convert.ToInt64(sina_id), next_cursor);
                        next_cursor = dt.next_cursor;
                        var dd = dt.users.Where(a => a.screen_name.Contains(key))
                          .Select(a => new { a.id, a.name, a.profile_image_url });
                        //.Take(PageSize)
                        if (dd.Count() == 0)
                        {
                        }
                        else
                        {
                            foreach (var item in dd)
                            {
                                FriendList fl = new FriendList();
                                fl.id = item.id.ToString();
                                fl.name = item.name.Length > 7 ? item.name.Substring(0, 7) + "..." : item.name;
                                fl.image_url = item.profile_image_url;
                                lfl.Add(fl);
                            }
                        }
                    }

                    if (lfl.Count == 0 || lfl == null)
                    {
                        _ErrorMessage = "没有相关内容的朋友!";
                    }
                    _IsOk = true;
                }
                else
                {
                    _IsOk = false;
                    _ErrorMessage = "亲~你凹凸曼了 新注册的微博吧！";
                }
            }
            else
            {
                _IsOk = false;
                _ErrorMessage = "你未登录，请登录在使用！";
            }
            data = new
            {
                friList = lfl,
                IsOk = _IsOk,
                ErrorMessage = _ErrorMessage
            };
            return JavaScriptConvert.SerializeObject(data);
        }

        public class FriendList
        {
            public string id { get; set; }
            public string name { get; set; }
            public string image_url { get; set; }
        }
    }
}