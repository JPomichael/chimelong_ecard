﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wbm.SinaV2API;

namespace chimelong_ECard_WEB.Expand
{
    public partial class sinalogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            oAuthSina oauth = SinaBase.oAuth();
            string link = oauth.GetAuthorization(); //获取用户认证地址
            Response.Redirect(link);
        }
    }
}