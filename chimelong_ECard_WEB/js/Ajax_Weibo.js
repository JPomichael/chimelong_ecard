﻿//
//  @ 深圳互动力
//  Session  login  return
//  By JPomichael 
//  2012/11/26
//

var sidone="";
var sidtwo="";
//页面载入启用
$(function () {
    GetSessionResult();
});

// 获取_Session。
function GetSessionResult() {
    var _url = "Expand/ajax_GetSession.aspx";
    var obj = {
        Type: "GetSession"
    }
    $.ajax({
        type: "POST",
        url: _url,
        data: obj,
        dataType: "json",
        success: function (data) {
            if (data != "") {
                ReplacementPolicy(data);
            }

        },
        timeout: 10000,
        error: function () {
            Timeout();
        }
    });
}

// 获取_Session后的替换
function ReplacementPolicy(data) {
    if (data.IsOk.toString() == "true") {
        if (data.Sei.name !== "") {
            //Sina User
            $(".login").hide();
            $(".tx img").attr("src", data.Sei.faceimage.toString());
            $(".user span").text(data.Sei.name);
            $(".exit").attr("href", "Expand/siginout.aspx");
            $(".exit").attr("title", "点击退出");
            $(".after_login").show();
        }
        else {
            //未登录
        }
    }
    else {
        alert("请求数据失败！");
    }
}


//发送微博
function SendWeibo() {
    $(".list").hide();
    $(".friends").html("");
    if ($("#txt").attr("value") == "To:" || $("#txt").attr("value") == "") {
        alert("送给ta一句或者一段祝福吧！");
    }
    else if ($("#txt").val().length > 90) {
        alert("最多可以书写90个字哦！");
    }
    else if ($("#sjr").html == "") {
        alert("亲 你还没有选择@的好友呢");
    }
    else if ($("#sjr a").length == 0) {
        alert("你还没有@好友呢！");
    }
    else if ($("#sjr a").length > 3) {
        alert("一次最多可@三位的好友！");
    }
    else if ($("#sjr a").length == 3||$("#sjr a").length <3) {
        var i, a, s = "";
        var id= "";
        var d=""; 
        a = document.getElementById("sjr").getElementsByTagName("A");
        for (i = 0; i < a.length; i++) {
                d=$(a[i]).attr("id").toString();
                id+= d+",";
        }
        var _url = "Expand/ajax_GetSession.aspx";
        var obj = {
            Type: "SendWeibo",
            Text: $("#txt").attr("value"),
            ID: id
        }
        $.ajax({
            type: "POST",
                url: _url,
                data: obj,
                dataType: "json",
                beforeSend: AjaxStart(),
                success: function (data) {
                    AjaxStop();
                    if (data != "") {
                        SendResult(data);
                    }
                },
                timeout: 10000,
                error: function () {
                    AjaxStop();
                    Timeout();
                }
            });
    }
}

function SendResult(data) {
    if (data.IsOk.toString() == "true") {
        if (data.IsWeiboOk.toString() == "true") {
            alert("发布成功！");
            $(".list").hide();
            $(".friends").html("");
            $(".sjr").html("");
            $("#txt").val("To:");
        }
        else {
            if (data.ErrorMessage.toString() != "") {
                alert(data.ErrorMessage.toString());
                $("a.xzhy").attr("href","#denglu");
            }
        }
    }
    else {
        alert("发布失败！失败原因：" + data.ErrorMessage.toString());
    }

}

//获取关注人列表
function SelFriend() {
    var _url = "Expand/ajax_GetSession.aspx";
    var obj = {
        Type: "GetFriend"
    }
    $.ajax({
        type: "POST",
        url: _url,
        data: obj,
        dataType: "json",
        beforeSend: Loading(),
        success: function (data) {
            if (data != "") {
                FriendReplacementPolicy(data);
            }
            else {
                alert("data==null");
            }
        },
        timeout: 10000,
        error: function () {
            Timeout();
        }
    });
}

//获得朋友列表的替换
function FriendReplacementPolicy(data) {
    var ShareHtml = "";
    if (data.IsOk.toString() == "true") {
        if (data.ErrorMessage!=="") {
            alert(data.ErrorMessage+"系统将会显示默认朋友列表!");
                 SelFriend(); //为空责是默认
        }
        else{
        ShareHtml += "<ul>";
        $.map(data.friList, function (fri) {
            ShareHtml += "<li>";
            ShareHtml += "<a href='javascript:;' id=" + fri.id + " ><img  style=\"height:40px;width:40px\" src=\"" + fri.image_url + "\" title=\"" + fri.name
             + "\"><span>" + fri.name + "</span></a>";
            ShareHtml += "</li>";
        });
        ShareHtml += "</ul>";
        $(".friends").html(ShareHtml);            
        }
    }
    else {
        if (data.ErrorMessage !== "") {
            alert(data.ErrorMessage);
            $(".list").hide();
             $("a.xzhy").attr("href","#denglu");
        }
    }
    //选择@好友
    var click = 1;
    $(".friends li").click(function () {
        var a_id = $(this).find("a").attr("id");
        var img_src = $(this).find("img").attr("src");
        var span_name = $(this).find("span").html();
        if ($("#sjr a").length == 3) { //够数了
            alert("一次最多可发送三位微博好友！亲下次吧!");
            return;
        }
        else if($("#sjr a").length==0) {//一个都木
            click=1;
            if (click == 1) {
                sidone = a_id;
                $(".sjr").append("<a id=" + a_id + " href='javascript:;'><img  style=\"height:35px;width:35px\" src=" + img_src + " title=" + span_name + "><img style=\"display: none;\" class=\"add_xx\" title=\"删除该好友\" src=\"images/XX.jpg\"></a>");
                click++;
            }
        }
        else if ($("#sjr a").length==1) {
            click=2;
            if (click == 2) {
                if (a_id == sidone) {
                    alert("不能多次选择同一新浪用户！");
                }
                else {
                    sidtwo = a_id;
                    $(".sjr").append("<a id=" + a_id + "  href='javascript:;'><img  style=\"height:35px;width:35px\" src=" + img_src + " title=" + span_name + "><img style=\"display: none;\" class=\"add_xx\" title=\"删除该好友\" src=\"images/XX.jpg\"></a>");
                    click++;
                }
            }
        }
        else if($("#sjr a").length==2) {
            click=3;
            if (click == 3) {
                if (sidone == a_id || sidtwo == a_id) {
                    alert("不能多次选择同一新浪用户！");
                }
                else {
                    $(".sjr").append("<a id=" + a_id + " href='javascript:;' ><img style=\"height:35px;width:35px\" src=" + img_src + " title=" + span_name + "><img style=\"display: none;\" class=\"add_xx\" title=\"删除该好友\" src=\"images/XX.jpg\"></a>");
                    click++;
                }
            }
        }
        else if ($("#sjr a").length==3 ){
            alert("一次最多可发送三位微博好友！亲下次吧!");
        }
        /*鼠标滑到收件人头像上出现 X*/
        $(".sjr a").mouseover(function(){
            $(this).find(".add_xx").show("slow");
        }).mouseleave(function(){
            $(this).find(".add_xx").hide("slow");
        });
        $(".add_xx").each(function(i){
            $(this).click(function(){
                click=i+1;
                var i, a, s = "";
                var idone, idtwo, idthree = "";
                a = document.getElementById("sjr").getElementsByTagName("A");
                for (i = 0; i < a.length; i++) {
                    if (i == 0) {
                        idone = $(a[i]).attr("id");
                    } 
                    else if (i == 1) {
                        idtwo = $(a[i]).attr("id");
                    } else if (i == 2) {
                        idthree = $(a[i]).attr("id");
                    }
                }
                if (click==1) {
                    if ($("#sjr a").length==1) {
                        sidone="";
                        sidtwo="";
                    }
                    else if ($("#sjr a").length==2) {
                        sidone=sidtwo;
                        sidtwo="";
                        alert(sidone,sidtwo);
                    }
                    else if ($("#sjr a").length==3) {
                        sidone=idtwo;
                        sidtwo=idthree;
                        alert(sidtwo);
                    }
                }
                else if (click==2){
                    if ($("#sjr a").length==1) {
                        
                    }
                    else if ($("#sjr a").length==2) {
                        sidone=idone;
                        sidtwo="";
                    }
                    else if ($("#sjr a").length==3) {
                        sidone=idone;
                        sidtwo= idthree;
                        alert(sidtwo);
                    }
                }
                else{
                        sidone=idone;
                        sidtwo=idtwo;
                }
                $(this).parent("a").remove();
            });
        })

        
    });
}




//根据关键字查找好友
function SelKeywords() {
    var _key = $("#SelFri").val();
    if (_key == " ") {
        SelFriend(); //为空责是默认
    }
    else if (_key.length > 10) {
        alert("亲 关键字很长很强大是么！简短点儿吧");
    }
    else {
        var _url = "Expand/ajax_GetSession.aspx";
        var obj = {
            Type: "SelFriend",
            Key: _key
        }
        $.ajax({
            type: "POST",
            url: _url,
            data: obj,
            dataType: "json",
            beforeSend: Loading(),
            success: function (data) {
                if (data != "") {
                    FriendReplacementPolicy(data);
                }
                else {
                    alert("没有搜索到相关的朋友，系统已帮你返回默认列表");
                    SelFriend(); //为空责是默认
                }
            },
            timeout: 20000,
            error: function () {
                Timeout();
            }
        });
    }
}

function Loading() {
    var Loading = "";
    Loading += "<div style=\"text-align: center; color:#886134; font-size:12px; padding-top: 40px;\">";
    Loading += "<img src=\"/images/loading.gif\"  /><br />";
    Loading += "加载中,请稍后...";
    Loading += "</div>";
    $(".friends").html(Loading);
}

function Timeout() {
    alert("请求数据超时了！请稍后重试");
}

//屏幕置灰效果
function AjaxStart() {
    $.blockUI({ message: '<img  src="/images/loading.gif" width="16"  height="16"/><font color="#2E6092" size="2">正在提交，稍候更精彩...</font>' });
}

//取消屏幕置灰效果
function AjaxStop() {
    $.unblockUI();
}
